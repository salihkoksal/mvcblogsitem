﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BlogSitem.Startup))]
namespace BlogSitem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
